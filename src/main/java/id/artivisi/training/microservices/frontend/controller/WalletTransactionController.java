package id.artivisi.training.microservices.frontend.controller;

import id.artivisi.training.microservices.frontend.dto.Wallet;
import id.artivisi.training.microservices.frontend.dto.WalletTransaction;
import id.artivisi.training.microservices.frontend.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WalletTransactionController {

    @Autowired private WalletService walletService;

    @GetMapping("/wallet/transaction")
    public ModelMap transaksiWallet() {
        Iterable<WalletTransaction> dataTransaksi
                = walletService.ambilDataTransaksiWallet("w001");
        for (WalletTransaction t : dataTransaksi) {
            System.out.println("Waktu Transaksi : "+t.getTransactionTime());
            System.out.println("Keterangan : "+t.getDescription());
            System.out.println("Nilai : "+t.getAmount());
        }
        return new ModelMap()
                .addAttribute("dataTransaksi", dataTransaksi);
    }
}
