package id.artivisi.training.microservices.frontend.controller;

import id.artivisi.training.microservices.frontend.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired private WalletService walletService;

    @GetMapping("/home")
    public ModelMap homepage() {
        return new ModelMap()
                .addAttribute("backendInfo", walletService.informasiHostBackend());
    }

}
